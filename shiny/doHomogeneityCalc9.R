# 
# 
# normVals <- DevelopNormalizedValues(weightDataFrame=weightDataFrame, paRawDF_UI=paRawDF)
# 
# 
# anova.out <- PerformANOVA(dat=normVals)
# 
# anova.out$SUMMARY
# anova.out$ANOVA.table
# anova.out$DATA
# anova.out$alpha
# 
# 
# doHomogeneityCalc9(ANOVAdata=anova.out)


doHomogeneityCalc9 <- function(ANOVAdata, input, values){
  
  tryCatch({
    summaryTable <- ANOVAdata$SUMMARY
    ANOVAtable <- ANOVAdata$ANOVA.table
    
    suppressWarnings(overallMean <- mean(summaryTable$Average)) # H7
    rootMS <- sqrt(ANOVAtable$MS[2]) # H8
    sbb  <- sqrt(abs(ANOVAtable$MS[1] - ANOVAtable$MS[2])/3) # H9
    ustarbb <- (sqrt(ANOVAtable$MS[2]/3)*(2/ANOVAtable$df[2])^0.25)/2 # H10
    # NOTE, n=3 here. Is that always the case or could n=4? Is this number of replicates in each group?
  
    rootMS_relative <- (rootMS/overallMean)*100
    sbb_relative <- (sbb/overallMean)*100     
    ustarbb_relative <- (ustarbb/overallMean)*100    # L10
    
    ubb <- NA
    if(  !(sum(ANOVAdata$DATA$Value[1:3], na.rm = TRUE) == 0)  )   ubb <- ustarbb_relative
    #IF(SUM(ANOVA!D9:D11)=0, "",L10) -- how should this be replicated? Actually test that sum=0 or do some other kind of data checking? *************
    
    return(list(overallMean=overallMean, rootMS=rootMS, sbb=sbb, ustarbb=ustarbb, rootMS_relative=rootMS_relative, sbb_relative=sbb_relative, ustarbb_relative=ustarbb_relative, ubb=ubb ))

  }, # End 'tryCatch' fxn expr
  
  error = function(e){
    # create error list with all inputs and save it
    errorList <- list("doHomogeneityCalc9", e, ANOVAdata, input, values)
    names(errorList) <- c("function","e", "ANOVAdata", "input", "values") 
    saveError(errorList)
    
    # return unsuccessful output
    return(list(overallMean = NA, rootMS = NA, sbb = NA, ustarbb = NA, 
                rootMS_relative = NA, sbb_relative = NA, ustarbb_relative = NA, ubb = NA))
    
  } # End 'tryCatch' error fxn
  ) # End 'tryCatch' fxn
} # End 'doHomogeneityCalc9' fxn
