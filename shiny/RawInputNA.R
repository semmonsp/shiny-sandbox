# this function checks to see if there are any NAs in E1-L3 samples in RawInput

RawInputNA <- function(RawInput){
  tryCatch({
    samples <- c("E1", "E2", "E3", "M1", "M2", "M3", "L1", "L2", "L3")
    RawInput <- RawInput[grepl(paste(samples, collapse = "|"), RawInput$Sample),]
    
    # remove columns that are all NA
    emptyCols <- list()
    #emptyCols[[1]] <- "Sample"
    for(i in 2:ncol(RawInput)){
      if(all(is.na(RawInput[, i]))){
        emptyCols[[i]] <- names(RawInput)[i]
      } # End 'if'
    } # End 'for-i-loop'
    
    RawInput <- RawInput[, !(names(RawInput) %in% unlist(emptyCols))]
    
    # make sure that Sample column is class character
    RawInput$Sample <- as.character(RawInput$Sample)
    
    return(all(!is.na(RawInput[,2:ncol(RawInput)])))
  }, # end 'expr' of 'tryCatch'
  error = function(e){
    return(TRUE)
  } # end 'error' function of 'tryCatch'
  ) # end 'tryCatch' function
} # end of function

