FROM semmons1/r-shinydriver

COPY . /
COPY ./shiny /shiny

EXPOSE 5000

# RUN Rscript -e 'source("shinyDeps.R")'
RUN Rscript -e 'source("RSelenium.R")'
