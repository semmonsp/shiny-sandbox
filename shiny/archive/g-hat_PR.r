
###Just that portion of Mest1B necessary to estimate g
#*** NOTE: I reduced the number of arguments and load those 
gHat <- function(
  searchType,
  radius,
  numTurbs,
  totalNumTurbs,
  k,      #fractional change in searcher efficiency with each search
  afa = NULL,  
  afb = NULL,  ##arrival distribution parameters, if not uniform dist
  season,
  searchInterval,
  phenologyOn,
  pcmWorkspaceFP, 
  ...
  ) 
  {
  #*** ADDITION: Load user defined PCM output workspace
    load(paste0(getwd(), pcmWorkspaceFP))
  
  #*** ADDITION: Globally assign necessary objects
    ifelse(exists("springArrivalProp"), assign("springArrivalProp", get("springArrivalProp"), pos = .GlobalEnv),
                                        assign("springArrivalProp", 0, pos = .GlobalEnv))
    ifelse(exists("summerArrivalProp"), assign("summerArrivalProp", get("summerArrivalProp"), pos = .GlobalEnv),
                                        assign("summerArrivalProp", 0, pos = .GlobalEnv))
    ifelse(exists("fallArrivalProp"), assign("fallArrivalProp", get("fallArrivalProp"), pos = .GlobalEnv),
                                      assign("fallArrivalProp", 0, pos = .GlobalEnv))
    ifelse(exists("winterArrivalProp"), assign("winterArrivalProp", get("winterArrivalProp"), pos = .GlobalEnv),
                                        assign("winterArrivalProp", 0, pos = .GlobalEnv))
    
    seasonalDurationDF <<- seasonalDurationDF
    
    if(phenologyOn == TRUE)
      {
      fn.arrivals <<- fn.arrivals_Pheno
      
      } else {
      fn.arrivals <<- fn.arrivals_noPheno
          
      } # End 'if/else'
    
  #*** ADDITION: Define f, pdb, pda, persistence distribution
    if(searchType == "roadPad")
      {
      f <- ifelse(exists(paste0("rp", season, "Seef", projectIn)), as.numeric(as.character(get(paste0("rp", season, "Seef", projectIn)))), 
                                                                   as.numeric(as.character(get(paste0("rpOverallSeef", projectIn)))))
      pdb <- ifelse(exists(paste0("rp", season, "Shape", projectIn)), as.numeric(as.character(get(paste0("rp", season, "Shape", projectIn)))), 
                                                                      as.numeric(as.character(get(paste0("rpOverallShape", projectIn)))))
      pda <- 1/as.numeric(as.character(get(paste0("overallScale", projectIn))))
      persistence_distn <- get(paste0("persistence_distn", projectIn))
      
      } else {
      f <- ifelse(exists(paste0("plot", season, "Seef", projectIn)), as.numeric(as.character(get(paste0("plot", season, "Seef", projectIn)))), 
                                                                     as.numeric(as.character(get(paste0("plotOverallSeef", projectIn)))))
      pdb <- ifelse(exists(paste0("plot", season, "Shape", projectIn)), as.numeric(as.character(get(paste0("plot", season, "Shape", projectIn)))), 
                                                                        as.numeric(as.character(get(paste0("plotOverallShape", projectIn)))))
      pda <- 1/as.numeric(as.character(get(paste0("overallScale", projectIn))))
      persistence_distn <- get(paste0("persistence_distn", projectIn))
        
      } # End 'if/else' 
  
  #*** ADDITION: Convert input paramaters to class numeric 
    radius <- as.numeric(as.character(radius))
    numTurbs <- as.numeric(as.character(numTurbs))
    totalNumTurbs <- as.numeric(as.character(totalNumTurbs))
    springArrivalProp <- as.numeric(as.character(springArrivalProp))
    summerArrivalProp <- as.numeric(as.character(summerArrivalProp))
    fallArrivalProp <- as.numeric(as.character(fallArrivalProp))
    winterArrivalProp <- as.numeric(as.character(winterArrivalProp))
    searchInterval <- as.numeric(as.character(searchInterval))
  
  #*** ADDITION: Determine days based on the seasonal information
    days <- seq(seasonalDurationDF[seasonalDurationDF$season == season, "seasonStart"], 
                seasonalDurationDF[seasonalDurationDF$season == season, "seasonEnd"], 
                searchInterval)
    
  #*** ADDITION: Define phi
    if(searchType == "roadPad")
      {
      dwpVal <- rpDWP$areaCorr[rpDWP$rad == radius]
      phi <- (numTurbs / totalNumTurbs) * dwpVal
      
      } else {
      dwpVal <- dwp$areaCorr[dwp$rad == radius]
      phi <- (numTurbs / totalNumTurbs) * dwpVal
        
      } # End 'if/else'
    
    
#*** NOTE: Added rnorm randomization below to give slightly non-Timber Road values for Cara's presentation
#*** NOTE: This must be removed for accurate results!!!
    
#   #*** TEMPORARY: Temp testing assigments
#     rpSeef <- f <- 0.99
#     phi <- 0.99
#     k <- 1.0
#     persistence_distn <- "exponential"
#     pda <- 1/100
    
   ## Now, get pr.1, which is P(seen in survey tt), for tt = 1, ... , 
   # nt
   nt<-length(days)-1 # number of sampling dates
   scale.init.1 <- numeric(nt)
   hold.tj <- matrix(0, nt, nt)
   # calculate probability carcass persists until sampling given 
      # sampling date
    for(tt in 1:nt)
      {
      for(j in 1:tt)
        {
        hold.tj[tt,j] <- (prod(1-f*k^(0:(tt-j-1)))^(j<tt)*f*k^(tt-j)*
                          fn.Psi.1(tt=tt,j=j,days=days, persistence_distn, pda, pdb)) # + rnorm(1, mean = 0, sd = 0.000001)

        }
      
      scale.init.1[tt] <- integrate(fn.arrivals, days[tt], days[tt+1])$val
      
      } # End for-tt-loop
   
    scale.init <- sum(scale.init.1)
    pr.t <- numeric(nt)
   
    for(tt in 1:nt)
      {
      pr.t[tt] <- phi*sum(hold.tj[tt,] / scale.init.1[tt])
      
      } # End for-tt-loop
   
  #*** TEMPORARY FIX: Remove NAs for 1-day search interval
  #*** TEMPORARY FIX: Need to talk to PR about better way
  #*** TEMPORARY FIX: This method has the benefit of being conservative
  #*** TEMPORARY FIX: as "g" will be a little lower than it should be
    # sum(pr.t * scale.init.1 / scale.init)
    sum(pr.t * scale.init.1 / scale.init, na.rm = TRUE)
    
  } ## end of function Mest1B (gHat)


### Functions internal to Mest functions
fn.Psi.1 <- function(tt,j,days,persistence_distn, pda, pdb){
  persistence_distn = tolower(persistence_distn)
  if (persistence_distn=="exponential"){
    ans <-integrate(int_Exp_Psi_0, days[j], days[j+1],tt=tt, 
      days=days,pda=pda,subdivisions=1000)$val
  }
  if (persistence_distn=="weibull"){
    ans <-integrate( int_Weib_Psi_0,days[j],days[j+1],tt=tt, 
      days=days, pda = pda, pdb = pdb,subdivisions=1000)$val
  }
  if (persistence_distn %in% c('loglogistic', "log-logistic")){
    ans <- integrate( int_LLogis_Psi_0,days[j],days[j+1], tt = tt, 
      days=days, pda= pda, pdb = pdb,subdivisions=1000)$val
  } 
  if (persistence_distn=="lognormal"){
    ans <- integrate( int_LNorm_Psi_0,days[j],days[j+1], tt = tt, 
      days=days, pda = pda, pdb = pdb,subdivisions=1000)$val
  }
  return(ans)
}

	
int_Exp_Psi_0 <-  function(z,tt, days, pda){
  return(pexp(days[tt+1]-z, pda, lower.tail=FALSE)* fn.arrivals(z))
}
int_Weib_Psi_0 <- function(z,tt, days, pda, pdb){
   return(pweibull(days[tt+1]-z, shape=pda, scale = pdb, 
    lower.tail=FALSE)* fn.arrivals(z))
}
int_LNorm_Psi_0 <- function(z,tt, days, pda, pdb){
  return(plnorm(days[tt+1]-z, meanlog=pdb, sdlog=sqrt(pda), 
    lower.tail=FALSE)  * fn.arrivals(z))
}
int_LLogis_Psi_0 <- function(z,tt, days, pda, pdb){
  return(pllogis(days[tt+1]-z, shape=pda, scale=pdb,lower.tail=FALSE) * 
    fn.arrivals(z))
}

#*** NOTE: I am using my own arrival function
##Assuming uniform arrival function
# fn.arrivals = function(duration) dunif(1:length(duration), 0, length(duration)) ##required for calc pi-hat