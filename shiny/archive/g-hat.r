
###Just that portion of Mest1B necessary to estimate g
gHat <- function(
  phi,    #### DWP
  f,      ###  SEEF
  k,      #fractional change in searcher efficiency with each search
  pda, pdb,  ##parameters for persistence distribution
  persistence_distn, ##Name of persistence distribution
  fn.arrivals,       ##arrival distribution
  days
  ) {
   ## Now, get pr.1, which is P(seen in survey tt), for tt = 1, ... , 
      # nt
   nt<-length(days)-1 # number of sampling dates
   scale.init.1 <- numeric(nt)
   hold.tj <- matrix(0, nt, nt)
   # calculate probability carcass persists until sampling given 
      # sampling date
   for(tt in 1:nt){
     for(j in 1:tt){
        hold.tj[tt,j] <- prod(1-f*k^(0:(tt-j-1)))^(j<tt)*f*k^(tt-j)*
          fn.Psi.1(tt=tt,j=j,days=days, persistence_distn, pda, pdb)
     }
     scale.init.1[tt] <- integrate(fn.arrivals, days[tt], 
      days[tt+1])$val
   }
   scale.init <- sum(scale.init.1)
   pr.t <- numeric(nt)
   
   # Problem is that pr.t[tt] being assigned many values when it should be
   # just getting one
   
   for(tt in 1:nt){
      pr.t[tt] <- phi*sum(hold.tj[tt,] / scale.init.1[tt])
   }
sum(pr.t * scale.init.1 / scale.init)
} ## end of function Mest1B


### Functions internal to Mest functions
fn.Psi.1 <- function(tt,j,days,persistence_distn, pda, pdb){
  persistence_distn = tolower(persistence_distn)
  if (persistence_distn=="exponential"){
    ans <-integrate(int_Exp_Psi_0, days[j], days[j+1],tt=tt, 
      days=days,pda=pda,subdivisions=1000)$val
  }
  if (persistence_distn=="weibull"){
    ans <-integrate( int_Weib_Psi_0,days[j],days[j+1],tt=tt, 
      days=days, pda = pda, pdb = pdb,subdivisions=1000)$val
  }
  if (persistence_distn %in% c('loglogistic', "log-logistic")){
    ans <- integrate( int_LLogis_Psi_0,days[j],days[j+1], tt = tt, 
      days=days, pda= pda, pdb = pdb,subdivisions=1000)$val
  } 
  if (persistence_distn=="lognormal"){
    ans <- integrate( int_LNorm_Psi_0,days[j],days[j+1], tt = tt, 
      days=days, pda = pda, pdb = pdb,subdivisions=1000)$val
  }
  return(ans)
}


int_Exp_Psi_0 <-  function(z,tt, days, pda){
  return(pexp(days[tt+1]-z, pda, lower.tail=FALSE)* fn.arrivals(z))
}
int_Weib_Psi_0 <- function(z,tt, days, pda, pdb){
   return(pweibull(days[tt+1]-z, shape=pda, scale = pdb, 
    lower.tail=FALSE)* fn.arrivals(z))
}
int_LNorm_Psi_0 <- function(z,tt, days, pda, pdb){
  return(plnorm(days[tt+1]-z, meanlog=pdb, sdlog=sqrt(pda), 
    lower.tail=FALSE)  * fn.arrivals(z))
}
int_LLogis_Psi_0 <- function(z,tt, days, pda, pdb){
  return(pllogis(days[tt+1]-z,shape=pda,scale=pdb,lower.tail=FALSE) * 
    fn.arrivals(z))
}



##Assuming uniform arrival function
fn.arrivals = function(duration) dunif(1:length(duration), 0, length(duration)) ##required for calc pi-hat
