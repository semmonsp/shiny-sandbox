# RawInput <- read.csv("L:/Zientek/shinyAT/Citalopram_Agilent_SingleSample.csv", check.names = FALSE, stringsAsFactors = FALSE)
# CalcPer <- read.csv("L:/Zientek/shinyAT/CalcPer.csv", check.names = FALSE, stringsAsFactors = FALSE)
# Normal <- read.csv("L:/Zientek/shinyAT/Normalization.csv", check.names = FALSE, stringsAsFactors = FALSE)
# WeightAver <- read.csv("L:/Zientek/shinyAT/WeightAver_2.csv", check.names = FALSE, stringsAsFactors = FALSE)
# 
# out <- CalcExtRes(RawInput, CalcPer, Normal, WeightAver)

CalcExtRes <- function(RawInput, CalcPer, Normal, WeightAver, ESA, samples, input, values){
  tryCatch({
    ExtInput <- RawInput
    
    # get all numeric columns
    for(i in 2:ncol(ExtInput)){
      ExtInput[[i]] <- as.numeric(as.character(ExtInput[[i]]))
    }
    
    names(ExtInput) <- c("Sample", gsub("_.*", "", names(RawInput)[names(RawInput) != "Sample"]))
    
    #subset down to only samples in selected sample set
    ExtInput <- ExtInput[grepl(paste(samples, collapse = "|"), ExtInput$Sample),]
    
    # create df with all samples that will have normalizations added to it from Normal
    NormalRaw <- data.frame(Sample = samples, Normalization = NA)
    
    if(length(samples) == 9){
      # define number of characters included in each sample
      sampNum <- 2
      # apply E normalization to all E samples
      NormalRaw[grepl("E", NormalRaw$Sample),]$Normalization <- Normal[Normal$Sample == "E", "Normalization"]
      # apply M normalization to all M samples
      NormalRaw[grepl("M", NormalRaw$Sample),]$Normalization <- Normal[Normal$Sample == "M", "Normalization"]
      # apply L normalization to all L samples
      NormalRaw[grepl("L", NormalRaw$Sample),]$Normalization <- Normal[Normal$Sample == "L", "Normalization"]
    }# end if
    
    if(length(samples) == 12){
      # define number of characters included in each sample
      sampNum <- 2
      # apply E normalization to all E samples
      NormalRaw[grepl("E", NormalRaw$Sample),]$Normalization <- Normal[Normal$Sample == "E", "Normalization"]
      # apply M normalization to all M samples
      NormalRaw[grepl("M", NormalRaw$Sample),]$Normalization <- Normal[Normal$Sample == "M", "Normalization"]
      # apply L normalization to all L samples
      NormalRaw[grepl("L", NormalRaw$Sample),]$Normalization <- Normal[Normal$Sample == "L", "Normalization"]
      # apply R normalization to all R samples
      NormalRaw[grepl("R", NormalRaw$Sample),]$Normalization <- Normal[Normal$Sample == "R", "Normalization"]
    }# end if
    
    if(length(samples) == 27){
      # define number of characters included in each sample
      sampNum <- 4
      for(i in samples){
        NormalRaw[grepl(i, NormalRaw$Sample),]$Normalization <- Normal[Normal$Sample == substr(i, 1, 2), "Normalization"]
      }#end for
    }# end if

    # get all samples with non NA normalizations
    NormSamps <- NormalRaw[!is.na(NormalRaw$Normalization), "Sample"]
    
    # remove "Default" from ESA
    ESA <- ESA[ESA$Chemical != "Default",]
    
    # convert ESA to character
    ESA$Chemical <- as.character(ESA$Chemical)
    ESA$`External Standard Applied` <- as.character(ESA$`External Standard Applied`)
    
    ## overwrite CalcPer with calculated % values based on ESA table
    CalcPerTmp <- CalcPer
    CalcPer$`Calculated %` <- NA
    
    # apply external standards defined
    for(i in ESA[ESA$`External Standard Applied` != "", "Chemical"]){
      CalcPer[CalcPer$Chemical == i, "Calculated %"] <- CalcPerTmp[CalcPerTmp$Chemical == ESA[ESA$Chemical == i, "External Standard Applied"], "Calculated %"]
    }
    
    # get chemicals with Calculated %'s
    PerChems <- CalcPer[!is.na(CalcPer$`Calculated %`), "Chemical"]
    
    # make everything characters
    WeightAver$Chemical <- as.character(WeightAver$Chemical)
    CalcPer$Chemical <- as.character(CalcPer$Chemical)
    NormSamps <- as.character(NormSamps)
    PerChems <- as.character(PerChems)
    
    Results <- ExtInput
    # set all values in every column (except Sample) to NA
    Results[,2:ncol(Results)] <- NA
    
    # get result for chemical x samples that have normaliaztion and calculated % values
    if(all(!is.na(ExtInput$Sample))){
      for(i in NormSamps){
        for(j in PerChems){
            Results[grepl(i, Results$Sample), j] <- ExtInput[grepl(i, ExtInput$Sample), j]*NormalRaw[NormalRaw$Sample == i, "Normalization"]/
              WeightAver[WeightAver$Chemical == j, "Average"]*CalcPer[CalcPer$Chemical == j, "Calculated %"]
        } # end 'for'
      } # end 'for'
    } # end 'if'
    
    # order samples for output
    # NOTE: The sample name needs to end with the sample number (i.e. E1-1)...this won't work unless that happens
    Results <- Results[match(samples, substr(Results$Sample, nchar(Results$Sample)-(sampNum-1), nchar(Results$Sample))),]
    
    return(Results)
  }, # end 'expr' of tryCatch function
  
  error = function(e){
    # create error list with all inputs and save it
    errorList <- list("CalcExtRes", e, RawInput, CalcPer, Normal, WeightAver, ESA, samples, input, values)
    names(errorList) <- c("function", "e", "RawInput", "CalcPer", "Normal", "WeightAver", "ESA", "samples", "input", "values") 
    saveError(errorList)
    
    # return unsuccessful output
    Results <- data.frame(Sample = NA, Chemical = NA)
    return(Results)
  } # end error function of tryCatch
  ) # end tryCatch function
} # end function
  


