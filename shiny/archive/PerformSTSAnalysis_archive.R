

# chemName <- "Cortisone Acetate"
# lotNum <- "LRAB1045"
# valueCompPct <- 100
# assayValPct <- 100
# ubb4 = NA; ubb9 = 0.067980269; ubb12 = NA
# weightDataFrame <- data.frame(
# Sample = c("E1", "E2", "E3", "E4", 
#                      "M1", "M2", "M3", "M4", 
#                      "L1", "L2", "L3", "L4",
#                      "USP1", "USP2", "USP3", "USP4",
#                      "EP1", "EP2", "EP3", "EP4",
#                      "BP1", "BP2", "BP3", "BP4",
#                      "STR1", "STR2", "STR3", "STR4"),
# Weight = c(10.19, 12.05, 11.08, 0, 
#                10.05, 10.03, 9.71, 0, 
#                9.7, 10.52, 12.02, 0,
#                10.09, 10.4, 10.57, 10.15,
#                11.12, 10.07, 10.2, 11.43,
#                9.88, 10.89, 10.58, 10.96,
#                11.7, 11.68, 9.85, 10.72),
# DilutionVolume = c(50, 50, 50, 0,
#                        50, 50, 50, 0,
#                        50, 50, 50, 0,
#                        50, 50, 50, 50,
#                        50, 50, 50, 50,
#                        50, 50, 50, 50,
#                        50, 50, 50, 50),
# TargetConcentration = c(0.2, 0.2, 0.2, 0,
#                 0.2, 0.2, 0.2, 0,
#                 0.2, 0.2, 0.2, 0,
#                 0.2, 0.2, 0.2, 0.2,
#                 0.2, 0.2, 0.2, 0.2,
#                 0.2, 0.2, 0.2, 0.2,
#                 0.2, 0.2, 0.2, 0.2), stringsAsFactors = FALSE)
# 
# unstressedNameVec <- c("E1-1", "E1-2", "E1-3", "M2-1", "M2-2", "M2-3",
#                        "L2-1", "L2-2", "L2-3", "L3-1", "L3-2", "L3-3")
# 
# crsSeqSummRepFP <- "Y:/MilliporeSigma/data/LRAB1045 Assay West_ADJ.xls"
# 
# tmpOut <- PerformSTSAnalysis(Sample = Sample, Weight = Weight, crsSeqSummRepFP = crsSeqSummRepFP, 
#                              chemName = chemName, lotNum = lotNum, valueCompPct = 100,
#                              DilutionVolume = DilutionVolume, TargetConcentration = TargetConcentration, ubb4 = NA, ubb9 = ubb9, ubb12 = NA, 
#                              expUncPHRCRM = 0, assayVal = 100,
#                              stressedNameVec = c("STR1-1", "STR1-2", "STR1-3", "STR2-1", "STR2-2", "STR2-3", 
#                                                  "STR3-1", "STR3-2", "STR3-3", "STR4-1", "STR4-2", "STR4-3"),
#                              kAlpha = 0.05)

PerformSTSAnalysis <- function(paDF = NULL, chemName, lotNum, valueCompPct = 100, weightDataFrame = NULL, # paRawDF_UI = NULL,
                               ubbPCT = NA, expUncPHRCRM = 0, assayVal = 100,
                               stressedNameVec = c("STR1-1", "STR1-2", "STR1-3", "STR2-1", "STR2-2", "STR2-3", 
                                                   "STR3-1", "STR3-2", "STR3-3", "STR4-1", "STR4-2", "STR4-3"),
                               unstressedNameVec,
                               kAlpha = 0.05)
  {
  # Deal with filepath not existing
    # if(length(crsSeqSummRepFP) < 1)
    # if(is.null(paRawDF_UI))
    #   {
    #   outDF <- data.frame(AssayType = "STS", yValue = NA, uCombK = NA, stringsAsFactors = FALSE)
    #   outLS <- list(AssayType = "STS", yVal = NA, uChar = NA, uBB = NA, uComb = NA, dfEff = NA, 
    #                 kVal = NA, yValue = NA, uCombK = NA, valueCompPct = NA,
    #                 assayVal = NA, stdUncPHRCRM = NA, kSqrt = NA, stabPHRCRM = NA, passFail = NA)
    #   outDFLS <- list(outDF, outLS)
    #   names(outDFLS) <- c("assayDF", "assayComps")
    #   return(outDFLS)
    #   
    #   } # End 'if'
  
  # Deal with an unformed assay DF
    if(is.null(paDF))
      {
      outDF <- data.frame(AssayType = "STS", yValue = NA, uCombK = NA, stringsAsFactors = FALSE)
      outLS <- list(AssayType = "STS", yVal = NA, uChar = NA, uBB = NA, uComb = NA, dfEff = NA, 
                    kVal = NA, yValue = NA, uCombK = NA, valueCompPct = NA, 
                    assayVal = NA, stdUncPHRCRM = NA, kSqrt = NA, stabPHRCRM = NA, passFail = NA)
      outDFLS <- list(outDF, outLS)
      names(outDFLS) <- c("assayDF", "assayComps")
      return(outDFLS)
      
      } # End 'if'
  
  # Deal with instances with no ubb PCT
    if(is.na(ubbPCT))
      {
      outDF <- data.frame(AssayType = "STS", yValue = NA, uCombK = NA, stringsAsFactors = FALSE)
      outLS <- list(AssayType = "STS", yVal = NA, uChar = NA, uBB = NA, uComb = NA, dfEff = NA, 
                    kVal = NA, yValue = NA, uCombK = NA, valueCompPct = NA, 
                    assayVal = NA, stdUncPHRCRM = NA, kSqrt = NA, stabPHRCRM = NA, passFail = NA)
      outDFLS <- list(outDF, outLS)
      names(outDFLS) <- c("assayDF", "assayComps")
      return(outDFLS)
      
      } # End 'if'
  
  # Deal with stressed or unstressed samples not being in the weights dataframe
    subSamps_stressed <- as.character(stressedNameVec)
    subSamps_unstressed <- as.character(unstressedNameVec)
    
    subSamps_stressedRev <- gsub("-[0-9]{1}$", "", subSamps_stressed, perl = TRUE)
    subSamps_unstressedRev <- gsub("-[0-9]{1}$", "", subSamps_unstressed, perl = TRUE)
    
    if(!all(subSamps_stressedRev %in% as.character(weightDataFrame$Sample)) | !all(subSamps_unstressedRev %in% as.character(weightDataFrame$Sample)))
      {
      subSamps_stressedRev %in% as.character(weightDataFrame$Sample)
      subSamps_unstressedRev %in% as.character(weightDataFrame$Sample)
      
      
      outDF <- data.frame(AssayType = "STS", yValue = NA, uCombK = NA, stringsAsFactors = FALSE)
      outLS <- list(AssayType = "STS", yVal = NA, uChar = NA, uBB = NA, uComb = NA, dfEff = NA, 
                    kVal = NA, yValue = NA, uCombK = NA, valueCompPct = NA,
                    assayVal = NA, stdUncPHRCRM = NA, kSqrt = NA, stabPHRCRM = NA, passFail = NA)
      outDFLS <- list(outDF, outLS)
      names(outDFLS) <- c("assayDF", "assayComps")
      return(outDFLS)
      
      } # End 'if'

  # Deal NA weights of stressed or unsetressed samples
    weightDataFrameRed <- weightDataFrame[weightDataFrame$Sample %in% c(subSamps_stressedRev, subSamps_unstressedRev), ]
    if(any(is.na(weightDataFrameRed$Weight)))
      {
      outDF <- data.frame(AssayType = "STS", yValue = NA, uCombK = NA, stringsAsFactors = FALSE)
      outLS <- list(AssayType = "STS", yVal = NA, uChar = NA, uBB = NA, uComb = NA, dfEff = NA, 
                    kVal = NA, yValue = NA, uCombK = NA, valueCompPct = NA,
                    assayVal = NA, stdUncPHRCRM = NA, kSqrt = NA, stabPHRCRM = NA, passFail = NA)
      outDFLS <- list(outDF, outLS)
      names(outDFLS) <- c("assayDF", "assayComps")
      return(outDFLS)
      
      } # End 'if'
      
  # Deal with weights of stressed or unsetressed samples being assigned as zero
    weightDataFrameRed <- weightDataFrame[weightDataFrame$Sample %in% c(subSamps_stressedRev, subSamps_unstressedRev), ]
    if(any(weightDataFrameRed$Weight == 0))
      {
      outDF <- data.frame(AssayType = "STS", yValue = NA, uCombK = NA, stringsAsFactors = FALSE)
      outLS <- list(AssayType = "STS", yVal = NA, uChar = NA, uBB = NA, uComb = NA, dfEff = NA, 
                    kVal = NA, yValue = NA, uCombK = NA, valueCompPct = NA,
                    assayVal = NA, stdUncPHRCRM = NA, kSqrt = NA, stabPHRCRM = NA, passFail = NA)
      outDFLS <- list(outDF, outLS)
      names(outDFLS) <- c("assayDF", "assayComps")
      return(outDFLS)
        
      } # End 'if'
    
  # Attach necessary package
    require(readxl)
  
  # Set options
    options(digits = 10)
    
  # # Develop normalizing factor
  #   paWtDF <- weightDataFrame
  #   paWtDF$normFactor <- paWtDF$TargetConcentration / (paWtDF$Weight / paWtDF$DilutionVolume) 
  #  
  # # Read in cross sequence summary report
  #   # paRawDF <- data.frame(read_excel(path = crsSeqSummRepFP, sheet = 1, skip = 4))
  #   paRawDF <- paRawDF_UI
  #   paRawDF <- paRawDF[!is.na(paRawDF[, 1]), ]
  #   paRawDF$sampleName <- as.character(unlist(data.frame(strsplit(paRawDF[, 1], "-"))[1, ]))
  #   
  #   paRawDFRed <- paRawDF
  #   paRawDFRed$sampleName <- NULL
  #   
  # # Convert raw data to normalized and restrict to stressed and unstressed samples
  #   peakAreaSampNamesNoRep <- gsub("-[0-9]{1}$", "", paRawDFRed[, 1], perl = TRUE)
  #   paRawDFRed$peakAreaSampNamesNoRep <- gsub("-[0-9]{1}$", "", paRawDFRed[, 1], perl = TRUE)
  #   
  #   paDF <- merge(paRawDFRed, paWtDF, by.x = "peakAreaSampNamesNoRep", by.y = "Sample", all.x = TRUE, sort = FALSE)
  #   # 
  #   # print("paRawDFRed")
  #   # print(head(paRawDFRed))
  #   # 
  #   # print("paWtDF")
  #   # print(head(paWtDF))
  #   # 
  #   # print("paDF")
  #   # print(head(paDF))
  #   
  #   peakAreaColInd <- grep("PeakArea", colnames(paDF))
  #   # paDF$Peak.Area <- as.numeric(as.character(paDF$Peak.Area))
  #   paDF$Peak.Area <- as.numeric(as.character(paDF[, peakAreaColInd]))
  #   paDF$peakAreaNorm <- paDF$Peak.Area * paDF$normFactor
    
    paDF$peakAreaSampNamesNoRepNoSampNum <- gsub("[0-9]", "", paDF$peakAreaSampNamesNoRep, perl = TRUE)
    paDF$peakAreaSampNamesNoRepNoSampNumGrp <- paDF$peakAreaSampNamesNoRepNoSampNum
    paDF_strs <- paDF[paDF[, 2] %in% stressedNameVec, ]
    paDF_unstrs <- paDF[paDF[, 2] %in% unstressedNameVec, ]
    
  # Calculate mean, stdev, and relative stdev for stressed and unstressed samples
    strsSummStats <- CalcSummStats(sampVec = paDF_strs$peakAreaNorm)
    unStrsSummStats <- CalcSummStats(sampVec = paDF_unstrs$peakAreaNorm)    
    
  # Complete STS calculations
    valueCompProp <- valueCompPct / 100
    yVal <- (strsSummStats$meanSamp / unStrsSummStats$meanSamp) * valueCompProp
    uChar <- sqrt((strsSummStats$relStdevSamp ^ 2) + (unStrsSummStats$relStdevSamp ^ 2)) * yVal
    uBB <- ubbPCT
    uComb <- sqrt((uChar ^ 2) + (uBB ^ 2))
    dfEff <- (uComb^4) / (strsSummStats$uDF + unStrsSummStats$uDF)
    kVal <- qt(p = 1 - (kAlpha / 2), df = dfEff)
    uCombKPct <- yCIPct <- uComb * kVal
    yValPct <- yVal * 100
    stdUncPHRCRM <- expUncPHRCRM / 2
    kSqrt <- (kVal * (sqrt((stdUncPHRCRM^2) + (uComb^2))))
    stabPHRCRM <- abs(assayVal - yValPct)
    passFail <- if(kSqrt <= stabPHRCRM){"Fail"} else {"Pass"}
    
  # Develop output dataframe and return
    outDF <- data.frame(AssayType = "STS", yValue = yValPct, uCombK = uCombKPct, stringsAsFactors = FALSE)
    outLS <- list(AssayType = "STS", yVal = yVal, uChar = uChar, uBB = uBB, uComb = uComb, dfEff = dfEff, 
                  kVal = kVal, yValue = yValPct, uCombK = uCombKPct, valueCompPct = valueCompPct,
                  assayVal = assayVal, stdUncPHRCRM = stdUncPHRCRM, kSqrt = kSqrt, stabPHRCRM = stabPHRCRM, passFail = passFail)
    outDFLS <- list(outDF, outLS, paDF)
    
    names(outDFLS) <- c("assayDF", "assayComps")
    return(outDFLS)
        
  } # End 'PerformSTSAnalysis' function
  